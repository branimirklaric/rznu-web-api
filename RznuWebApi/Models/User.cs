namespace RznuWebApi {
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Runtime.Serialization;
    using System.Xml.Serialization;

    [Table("User")]
    public partial class User {
        private string username;
        private string password;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public User() {
            Statuses = new HashSet<Status>();
        }

        public int Id { get; set; }

        [Required]
        [StringLength(40)]
        public string Username {
            get {
                return username.TrimEnd();
            }
            set {
                username = value;
            }
        }

        [Required]
        [StringLength(40)]
        public string Password {
            get {
                return password.TrimEnd();
            }
            set {
                password = value;
            }
        }

        [XmlIgnore]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual HashSet<Status> Statuses { get; set; }

        public override bool Equals(object obj) {
            var user = obj as User;
            if (user == null) {
                return false;
            }
            var sameId = Id.Equals(user.Id);
            var sameUsername = Username.Equals(user.Username.TrimEnd());
            var samePassword = Password.Equals(user.Password.TrimEnd());
            return sameId && sameUsername && samePassword;
        }
    }
}
