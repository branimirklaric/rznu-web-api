namespace RznuWebApi {
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Runtime.Serialization;
    using System.Xml.Serialization;

    public partial class Status {
        private string text;

        public int Id { get; set; }

        public int UserId { get; set; }

        [Required]
        [StringLength(255)]
        public string Text {
            get {
                return text.TrimEnd();
            }
            set {
                text = value;
            }
        }

        [XmlIgnore]
        public virtual User User { get; set; }

        public override bool Equals(object obj) {
            var status = obj as Status;
            if (status == null) {
                return false;
            }
            var sameId = Id.Equals(status.Id);
            var sameUserId = UserId.Equals(status.UserId);
            var sameText = Text.Equals(status.Text);
            return sameId && sameUserId && sameText;
        }
    }
}
