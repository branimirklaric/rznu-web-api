﻿using System.IO;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Web;
using System.Web.Configuration;
using System.Collections;
using System.Collections.Specialized;
using System;

namespace RznuWebApi {
	public class RequestUriPathLogger : MessageProcessingHandler {
		protected override HttpRequestMessage ProcessRequest(
				HttpRequestMessage request, CancellationToken cancellationToken) {
            string browserBrand;
            try {
                browserBrand = HttpContext.Current.Request.Browser.Browser.ToLower();
            }
            catch (Exception) {
                browserBrand = "test";
            }
			
            using (StreamWriter file = new StreamWriter(
                    @"D:\Solutions\RZNU\1_labos\1_labos\log.txt", true)) {
				file.WriteLine(request.RequestUri.AbsolutePath + "\t" + browserBrand);
			}
			return request;
		}

		protected override HttpResponseMessage ProcessResponse(
				HttpResponseMessage response, CancellationToken cancellationToken) {
			return response;
		}
	}
}