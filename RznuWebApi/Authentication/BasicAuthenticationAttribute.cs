﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Filters;
using System.Web.Http.Results;

namespace RznuWebApi {
	public class BasicAuthenticationAttribute : Attribute, IAuthenticationFilter {
		public async Task AuthenticateAsync(HttpAuthenticationContext context, 
				CancellationToken cancellationToken) {
			cancellationToken.ThrowIfCancellationRequested();

			HttpRequestMessage request = context.Request;
			AuthenticationHeaderValue authorization = request.Headers.Authorization;

			if (authorization == null || authorization.Scheme != "Basic"
                    || string.IsNullOrEmpty(authorization.Parameter)) {
				return;
			}

			Tuple<string, string> userNameAndPasword = 
				ExtractUserNameAndPassword(authorization.Parameter);

			if (userNameAndPasword == null) {
				return;
			}

			string userName = userNameAndPasword.Item1;
			string password = userNameAndPasword.Item2;

			IPrincipal principal = Authenticate(userName, password);

			if (principal == null) {
			}
			else {
				context.Principal = principal;
			}
		}

		private IPrincipal Authenticate(string username, string password) {
            var db = new UsersAndStatusesContext();
            var allUsers = db.User.ToList();
            var authenticated = false;
            foreach (var user in allUsers) {
                var equalUsername = username.Equals(user.Username);
                var equalPassword = password.Equals(user.Password);
                if (equalUsername && equalPassword) {
                    authenticated = true;
                    break;
                }
            }
			if (!authenticated) {
				return null;
			}
			ClaimsIdentity identity = new ClaimsIdentity("Basic");
		    var principal = new ClaimsPrincipal(identity);
			return principal;
		}

		private static Tuple<string, string> ExtractUserNameAndPassword(
				string authorizationParameter) {
			byte[] credentialBytes = Convert.FromBase64String(authorizationParameter);
			string decodedCredentials = Encoding.ASCII.GetString(credentialBytes);
			if (string.IsNullOrEmpty(decodedCredentials)) {
				return null;
			}

			int colonIndex = decodedCredentials.IndexOf(':');
			if (colonIndex == -1) {
				return null;
			}

			string userName = decodedCredentials.Substring(0, colonIndex);
			string password = decodedCredentials.Substring(colonIndex + 1);
			return new Tuple<string, string>(userName, password);
		}

		public Task ChallengeAsync(HttpAuthenticationChallengeContext context, 
				CancellationToken cancellationToken) {
			if (!context.ActionContext.RequestContext.Principal.Identity.IsAuthenticated) {
				var challenges = new List<AuthenticationHeaderValue>();
				challenges.Add(new AuthenticationHeaderValue("Basic"));
				context.Result = new UnauthorizedResult(challenges, context.Request);
			}
			return Task.FromResult(0);
		}

		public virtual bool AllowMultiple {
			get { return false; }
		}
	}
}