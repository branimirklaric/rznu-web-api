namespace RznuWebApi {
	using System;
	using System.Data.Entity;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	public partial class UsersAndStatusesContext : DbContext {
		public UsersAndStatusesContext() : base("name=RealDb") {
            Configuration.ProxyCreationEnabled = false;
        }

        public UsersAndStatusesContext(string dbName) : base("name=" + dbName) {
            Configuration.ProxyCreationEnabled = false;
        }

		public virtual DbSet<Status> Status { get; set; }
		public virtual DbSet<User> User { get; set; }

		protected override void OnModelCreating(DbModelBuilder modelBuilder) {
			modelBuilder.Entity<Status>()
				.Property(e => e.Text)
				.IsFixedLength();

			modelBuilder.Entity<User>()
				.Property(e => e.Username)
				.IsFixedLength();

			modelBuilder.Entity<User>()
				.Property(e => e.Password)
				.IsFixedLength();

			modelBuilder.Entity<User>()
				.HasMany(e => e.Statuses)
				.WithRequired(e => e.User)
				.WillCascadeOnDelete(false);
		}
	}
}
