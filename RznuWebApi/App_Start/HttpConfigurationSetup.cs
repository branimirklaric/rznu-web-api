﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace RznuWebApi {
    public static class HttpConfigurationSetup {
        public static HttpConfiguration SetupConfiguration(
                this HttpConfiguration config, bool test) {
            config.Formatters.XmlFormatter.UseXmlSerializer = true;
            config.MessageHandlers.Add(new RequestUriPathLogger());
            config.IncludeErrorDetailPolicy = IncludeErrorDetailPolicy.Always;

            config.MapHttpAttributeRoutes();
            return config;
        }
    }
}