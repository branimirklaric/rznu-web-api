﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

namespace RznuWebApi.Controllers {
	public class DocumentationController : ApiController {
		// GET: api/Default
        [Route("api")]
		public HttpResponseMessage Get() {
            var response = new HttpResponseMessage();            

            var apiExplorer = GlobalConfiguration.Configuration.Services.GetApiExplorer();
            var content = "<h3>APIs</h3>\n<ul>\n";
            foreach (var api in apiExplorer.ApiDescriptions) {
                content += string.Format(
                    "<li>\n<h5>{0} {1}</h5>\n", api.HttpMethod, api.RelativePath);
                content += string.Format("<p>{0}.{1}</p><blockquote>\n",
                    api.ActionDescriptor.ControllerDescriptor.ControllerName,
                    api.ActionDescriptor.ActionName);
                if (api.ParameterDescriptions.Count > 0) {
                    content += "<h6>Parameters</h6>\n";
                    content += "<ul>\n";
                    foreach (var parameter in api.ParameterDescriptions) {
                        content += string.Format("<li>{0} ({1})</li>\n",
                            parameter.Name, parameter.Source);
                    }
                    content += "</ul>\n";
                }
                content += "</blockquote></li><hr>\n";
            }
            content += "</ul>";
            response.Content = new StringContent(content);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");

            return response;
        }
	}
}
