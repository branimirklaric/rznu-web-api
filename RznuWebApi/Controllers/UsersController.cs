﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using RznuWebApi;
using System.Xml.Serialization;
using System.IO;
using System.Xml;
using System.Web;

namespace RznuWebApi.Controllers {
	[BasicAuthentication]
    [RoutePrefix("api/Users")]
    public class UsersController : ApiController {
		private UsersAndStatusesContext db;

        public UsersController() {
            db = new UsersAndStatusesContext();
        }

        public UsersController(string dbName) {
            db = new UsersAndStatusesContext(dbName);
        }

		// GET: api/Users
        [Route("")]
		public IHttpActionResult GetUsers() {
			return Ok(db.User.ToList());
		}

		// GET: api/Users/5
		[ResponseType(typeof(User))]
        [Route("{id:int}")]
		public IHttpActionResult GetUser(int id) {
			User user = db.User.Find(id);
			if (user == null) {
				return NotFound();
			}

			return Ok(user);
		}

		// PUT: api/Users/5
		[ResponseType(typeof(void))]
        [Route("{id:int}", Name = "GetUser")]
        public IHttpActionResult PutUser(int id, User user) {
			if (!ModelState.IsValid) {
				return BadRequest(ModelState);
			}

			if (id != user.Id) {
				return BadRequest();
			}

			db.Entry(user).State = EntityState.Modified;

			try {
				db.SaveChanges();
			}
			catch (DbUpdateConcurrencyException) {
				if (!UserExists(id)) {
					return NotFound();
				}
				else {
					throw;
				}
			}

			return StatusCode(HttpStatusCode.NoContent);
		}

		// POST: api/Users
		[ResponseType(typeof(User))]
        [Route("")]
        public IHttpActionResult PostUser(User user) {
			if (!ModelState.IsValid) {
				return BadRequest(ModelState);
			}

			db.User.Add(user);
			db.SaveChanges();

			return CreatedAtRoute("GetUser", new { id = user.Id }, user);
		}

		// DELETE: api/Users/5
		[ResponseType(typeof(User))]
        [Route("{id:int}")]
        public IHttpActionResult DeleteUser(int id) {
			User user = db.User.Find(id);
			if (user == null) {
				return NotFound();
			}

			db.User.Remove(user);
			db.SaveChanges();

			return Ok(user);
		}

		[HttpGet]
        [Route("{id:int}/Statuses")]
        public IHttpActionResult Statuses(int id) {
			return Ok(db.Status.Where(s => s.UserId == id).ToList());
		}

		protected override void Dispose(bool disposing) {
			if (disposing) {
				db.Dispose();
			}
			base.Dispose(disposing);
		}

		private bool UserExists(int id) {
			return db.User.Count(e => e.Id == id) > 0;
		}
	}
}