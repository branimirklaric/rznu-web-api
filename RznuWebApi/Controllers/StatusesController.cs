﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using RznuWebApi;

namespace RznuWebApi.Controllers {
	[BasicAuthentication]
    [RoutePrefix("api/Statuses")]
    public class StatusesController : ApiController {
        private UsersAndStatusesContext db;

        public StatusesController() {
            db = new UsersAndStatusesContext();
        }

        public StatusesController(string dbName) {
            db = new UsersAndStatusesContext(dbName);
		}

        // GET: api/Statuses
        [Route("")]
        public IEnumerable<Status> GetStatuses() {
			return db.Status.ToList();
		}

		// GET: api/Statuses/5
		[ResponseType(typeof(Status))]
        [Route("{id:int}", Name = "GetStatus")]
        public IHttpActionResult GetStatus(int id) {
			Status status = db.Status.Find(id);
			if (status == null) {
				return NotFound();
			}

			return Ok(status);
		}

		// PUT: api/Statuses/5
		[ResponseType(typeof(void))]
        [Route("{id:int}")]
        public IHttpActionResult PutStatus(int id, Status status) {
			if (!ModelState.IsValid) {
				return BadRequest(ModelState);
			}

			if (id != status.Id) {
				return BadRequest();
			}

			db.Entry(status).State = EntityState.Modified;

			try {
				db.SaveChanges();
			}
			catch (DbUpdateConcurrencyException) {
				if (!StatusExists(id)) {
					return NotFound();
				}
				else {
					throw;
				}
			}

			return StatusCode(HttpStatusCode.NoContent);
		}

		// POST: api/Statuses
		[ResponseType(typeof(Status))]
        [Route("")]
        public IHttpActionResult PostStatus(Status status) {
			if (!ModelState.IsValid) {
				return BadRequest(ModelState);
			}

			db.Status.Add(status);
			db.SaveChanges();

			return CreatedAtRoute("GetStatus", new { id = status.Id }, status);
		}

		// DELETE: api/Statuses/5
		[ResponseType(typeof(Status))]
        [Route("{id:int}")]
        public IHttpActionResult DeleteStatus(int id) {
			Status status = db.Status.Find(id);
			if (status == null) {
				return NotFound();
			}

			db.Status.Remove(status);
			db.SaveChanges();

			return Ok(status);
		}

		protected override void Dispose(bool disposing) {
			if (disposing) {
				db.Dispose();
			}
			base.Dispose(disposing);
		}

		private bool StatusExists(int id) {
			return db.Status.Count(e => e.Id == id) > 0;
		}
	}
}