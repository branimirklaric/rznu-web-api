﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

namespace RznuWebApi.Controllers {
    public class AddressVisitsController : ApiController {
        // GET: api/Default
        [Route("stats/address")]
        public HttpResponseMessage Get() {
            var file = 
                new StreamReader(@"D:\Solutions\RZNU\1_labos\1_labos\address.txt");
            var content = file.ReadToEnd().Replace("\n", "<br>");
            file.Close();

            var response = new HttpResponseMessage(HttpStatusCode.OK);
            response.Content = new StringContent(content);
            response.Content.Headers.ContentType = 
                new MediaTypeHeaderValue("text/html");

            return response;
        }
    }
}
