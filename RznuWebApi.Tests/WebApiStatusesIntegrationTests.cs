﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RznuWebApi;
using System.Web.Http;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

namespace RznuWebApi.Tests {
    [TestClass]
    public class WebApiStatusesIntegrationTests {
        private HttpServer server;
        private UsersAndStatusesContext db = new UsersAndStatusesContext();

        public WebApiStatusesIntegrationTests() {
            AppDomain.CurrentDomain.SetData(
                "DataDirectory", @"D:\Solutions\RZNU\1_labos\1_labos\App_Data\");
            var config = new HttpConfiguration().SetupConfiguration(true);
            server = new HttpServer(config);
        }

        private HttpRequestMessage GetHttpRequest(HttpMethod method, string url) {
            var request = new HttpRequestMessage(method, "http://asdf.com/" + url);
            request.Headers.Accept
                .Add(new MediaTypeWithQualityHeaderValue("application/xml"));
            return request;
        }

        [TestMethod]
        public void UnauthorizedStatus() {
            var client = new HttpClient(server);
            var httpRequestMessage = GetHttpRequest(HttpMethod.Get, "api/Statuses");

            using (var response = client.SendAsync(httpRequestMessage).Result) {
                Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
            }
            httpRequestMessage.Dispose();
        }

        [TestMethod]
        public void GetAllStatuses() {
            var client = new HttpClient(server);
            var httpRequestMessage = GetHttpRequest(HttpMethod.Get, "api/Statuses");
            httpRequestMessage.Headers.Authorization
                = new AuthenticationHeaderValue("Basic", "TmVraUxpazoxMjM0");

            var allStatuses = db.Status.ToList();
            using (var response = client.SendAsync(httpRequestMessage).Result) {
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                var responseStatuses = 
                    response.Content.ReadAsAsync<List<Status>>().Result;
                Assert.IsTrue(allStatuses.SequenceEqual(responseStatuses));
            }
            httpRequestMessage.Dispose();
        }

        [TestMethod]
        public void GetStatus() {
            var client = new HttpClient(server);
            var httpRequestMessage = GetHttpRequest(HttpMethod.Get, "api/Statuses/1");
            httpRequestMessage.Headers.Authorization
                = new AuthenticationHeaderValue("Basic", "TmVraUxpazoxMjM0");

            var status = db.Status.Where(s => s.Id == 1).Single();

            using (var response = client.SendAsync(httpRequestMessage).Result) {
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                var responseStatus = response.Content.ReadAsAsync<Status>().Result;
                Assert.AreEqual(status, responseStatus);
            }
            httpRequestMessage.Dispose();
        }

        [TestMethod]
        public void PostStatus() {
            var client = new HttpClient(server);
            var httpRequestMessage = GetHttpRequest(HttpMethod.Post, "api/Statuses");
            httpRequestMessage.Headers.Authorization
                = new AuthenticationHeaderValue("Basic", "TmVraUxpazoxMjM0");

            var keyValuePairs = new List<KeyValuePair<string, string>>();
            keyValuePairs.Add(new KeyValuePair<string, string>("UserId", "1"));
            keyValuePairs.Add(new KeyValuePair<string, string>("Text", "Hue"));
            httpRequestMessage.Content = new FormUrlEncodedContent(keyValuePairs);

            using (var response = client.SendAsync(httpRequestMessage).Result) {
                Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
                var responseStatus = response.Content.ReadAsAsync<Status>().Result;
                var insertedStatus = db.Status.Find(responseStatus.Id);
                Assert.IsNotNull(insertedStatus);
                db.Status.Remove(insertedStatus);
                db.SaveChanges();
                Assert.AreEqual(insertedStatus, responseStatus);
                Assert.AreEqual(insertedStatus.UserId, 1);
                Assert.AreEqual(insertedStatus.Text, "Hue");
            }
            httpRequestMessage.Dispose();
        }

        [TestMethod]
        public void PutStatus() {
            var client = new HttpClient(server);
            var httpRequestMessage = GetHttpRequest(HttpMethod.Put, "api/Statuses/5");
            httpRequestMessage.Headers.Authorization
                = new AuthenticationHeaderValue("Basic", "TmVraUxpazoxMjM0");

            var keyValuePairs = new List<KeyValuePair<string, string>>();
            keyValuePairs.Add(new KeyValuePair<string, string>("Id", "5"));
            keyValuePairs.Add(new KeyValuePair<string, string>("UserId", "1"));
            keyValuePairs.Add(new KeyValuePair<string, string>("Text", "Hue"));
            httpRequestMessage.Content = new FormUrlEncodedContent(keyValuePairs);

            using (var response = client.SendAsync(httpRequestMessage).Result) {
                Assert.AreEqual(HttpStatusCode.NoContent, response.StatusCode);
                var changedStatus = db.Status.Find(5);
                Assert.IsNotNull(changedStatus);
                Assert.AreEqual(changedStatus.Id, 5);
                Assert.AreEqual(changedStatus.UserId, 1);
                Assert.AreEqual(changedStatus.Text, "Hue");
                changedStatus.Text = "Test";
                db.Entry(changedStatus).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            httpRequestMessage.Dispose();
        }

        [TestMethod]
        public void DeleteStatus() {
            var client = new HttpClient(server);
            var httpRequestMessage = GetHttpRequest(HttpMethod.Delete, "api/Statuses/5");
            httpRequestMessage.Headers.Authorization
                = new AuthenticationHeaderValue("Basic", "TmVraUxpazoxMjM0");

            using (var response = client.SendAsync(httpRequestMessage).Result) {
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                var deletedStatus = db.User.Find(5);
                Assert.IsNull(deletedStatus);
                var responseUser = response.Content.ReadAsAsync<Status>().Result;
                Assert.AreEqual(responseUser.Id, 5);
                Assert.AreEqual(responseUser.UserId, 1);
                Assert.AreEqual(responseUser.Text, "Test");

                SqlConnection sqlConnection = new SqlConnection(
                    ConfigurationManager.ConnectionStrings["RealDb"].ConnectionString);
                SqlCommand cmd = new SqlCommand();

                cmd.CommandText = "SET IDENTITY_INSERT [Status] ON;"
                    + "INSERT INTO [Status] (Id, UserId, Text)"
                    + "VALUES (5, 1, 'Test');";
                cmd.CommandType = CommandType.Text;
                cmd.Connection = sqlConnection;

                sqlConnection.Open();
                cmd.ExecuteNonQuery();
                sqlConnection.Close();

            }
            httpRequestMessage.Dispose();
        }
    }
}
