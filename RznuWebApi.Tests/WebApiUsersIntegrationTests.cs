﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RznuWebApi;
using System.Web.Http;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

namespace RznuWebApi.Tests {
	[TestClass]
	public class WebApiUsersIntegrationTests {
        private HttpServer server;
        private UsersAndStatusesContext db = new UsersAndStatusesContext();

        public WebApiUsersIntegrationTests() {
            AppDomain.CurrentDomain.SetData(
                "DataDirectory", @"D:\Solutions\RZNU\1_labos\1_labos\App_Data\");
            var config = new HttpConfiguration().SetupConfiguration(true);
            server = new HttpServer(config);
        }

        private HttpRequestMessage GetHttpRequest(HttpMethod method, string url) {
            var request = new HttpRequestMessage(method, "http://asdf.com/" + url);
            request.Headers.Accept
                .Add(new MediaTypeWithQualityHeaderValue("application/xml"));
            return request;
        }

		[TestMethod]
		public void UnauthorizedStatus() {
            var client = new HttpClient(server);
            var httpRequestMessage = GetHttpRequest(HttpMethod.Get, "api/Users");

            using (var response = client.SendAsync(httpRequestMessage).Result) {
                Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
            }
            httpRequestMessage.Dispose();
		}

        [TestMethod]
        public void GetAllUsers() {
            var client = new HttpClient(server);
            var httpRequestMessage = GetHttpRequest(HttpMethod.Get, "api/Users");
            httpRequestMessage.Headers.Authorization 
                = new AuthenticationHeaderValue("Basic", "TmVraUxpazoxMjM0");

            var allUsers = db.User.ToList();
            using (var response = client.SendAsync(httpRequestMessage).Result) {
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                var responseUsers = response.Content.ReadAsAsync<List<User>>().Result;
                Assert.IsTrue(allUsers.SequenceEqual(responseUsers));
            }
            httpRequestMessage.Dispose();
        }

        [TestMethod]
        public void GetUser() {
            var client = new HttpClient(server);
            var httpRequestMessage = GetHttpRequest(HttpMethod.Get, "api/Users/1");
            httpRequestMessage.Headers.Authorization
                = new AuthenticationHeaderValue("Basic", "TmVraUxpazoxMjM0");

            var user = db.User.Where(u => u.Id == 1).Single();

            using (var response = client.SendAsync(httpRequestMessage).Result) {
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                var responseUser = response.Content.ReadAsAsync<User>().Result;
                Assert.AreEqual(user, responseUser);
            }
            httpRequestMessage.Dispose();
        }

        [TestMethod]
        public void PostUser() {
            var client = new HttpClient(server);
            var httpRequestMessage = GetHttpRequest(HttpMethod.Post, "api/Users");
            httpRequestMessage.Headers.Authorization
                = new AuthenticationHeaderValue("Basic", "TmVraUxpazoxMjM0");

            var keyValuePairs = new List<KeyValuePair<string, string>>();
            keyValuePairs.Add(new KeyValuePair<string, string>("Username", "TestUser"));
            keyValuePairs.Add(new KeyValuePair<string, string>("Password", "TestPass"));
            httpRequestMessage.Content = new FormUrlEncodedContent(keyValuePairs);
                       
            using (var response = client.SendAsync(httpRequestMessage).Result) {
                Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
                var responseUser = response.Content.ReadAsAsync<User>().Result;
                var insertedUser = db.User.Find(responseUser.Id);
                Assert.IsNotNull(insertedUser);
                db.User.Remove(insertedUser);
                db.SaveChanges();
                Assert.AreEqual(insertedUser, responseUser);
                Assert.AreEqual(insertedUser.Username, "TestUser");
                Assert.AreEqual(insertedUser.Password, "TestPass");
            }
            httpRequestMessage.Dispose();
        }

        [TestMethod]
        public void PutUser() {
            var client = new HttpClient(server);
            var httpRequestMessage = GetHttpRequest(HttpMethod.Put, "api/Users/17");
            httpRequestMessage.Headers.Authorization
                = new AuthenticationHeaderValue("Basic", "TmVraUxpazoxMjM0");

            var keyValuePairs = new List<KeyValuePair<string, string>>();
            keyValuePairs.Add(new KeyValuePair<string, string>("Id", "17"));
            keyValuePairs.Add(
                new KeyValuePair<string, string>("Username", "ChangedTest"));
            keyValuePairs.Add(
                new KeyValuePair<string, string>("Password", "ChangedPass"));
            httpRequestMessage.Content = new FormUrlEncodedContent(keyValuePairs);

            using (var response = client.SendAsync(httpRequestMessage).Result) {
                Assert.AreEqual(HttpStatusCode.NoContent, response.StatusCode);
                var changedUser = db.User.Find(17);
                Assert.IsNotNull(changedUser);
                Assert.AreEqual(changedUser.Id, 17);
                Assert.AreEqual(changedUser.Username, "ChangedTest");
                Assert.AreEqual(changedUser.Password, "ChangedPass");
                changedUser.Username = "PermanentTest";
                changedUser.Password = "PermanentPass";
                db.Entry(changedUser).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            httpRequestMessage.Dispose();
        }

        [TestMethod]
        public void DeleteUser() {
            var client = new HttpClient(server);
            var httpRequestMessage = GetHttpRequest(HttpMethod.Delete, "api/Users/17");
            httpRequestMessage.Headers.Authorization
                = new AuthenticationHeaderValue("Basic", "TmVraUxpazoxMjM0");

            using (var response = client.SendAsync(httpRequestMessage).Result) {
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                var deletedUser = db.User.Find(17);
                Assert.IsNull(deletedUser);
                var responseUser = response.Content.ReadAsAsync<User>().Result;
                Assert.AreEqual(responseUser.Id, 17);
                Assert.AreEqual(responseUser.Username, "PermanentTest");
                Assert.AreEqual(responseUser.Password, "PermanentPass");

                SqlConnection sqlConnection = new SqlConnection(
                    ConfigurationManager.ConnectionStrings["RealDb"].ConnectionString);
                SqlCommand cmd = new SqlCommand();

                cmd.CommandText = "SET IDENTITY_INSERT [User] ON;"
                    + "INSERT INTO [User] (Id, Username, Password)"
                    + "VALUES (17, 'PermanentUser', 'PermanentPass');";
                cmd.CommandType = CommandType.Text;
                cmd.Connection = sqlConnection;

                sqlConnection.Open();
                cmd.ExecuteNonQuery();
                sqlConnection.Close();

            }
            httpRequestMessage.Dispose();
        }

        [TestMethod]
        public void GetAllStatusesOfUser() {
            var client = new HttpClient(server);
            var httpRequestMessage =
                GetHttpRequest(HttpMethod.Get, "api/Users/1/Statuses");
            httpRequestMessage.Headers.Authorization
                = new AuthenticationHeaderValue("Basic", "TmVraUxpazoxMjM0");

            var allStatusesOfUser = db.Status.Where(s => s.UserId == 1).ToList();
            using (var response = client.SendAsync(httpRequestMessage).Result) {
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                var responseStatuses =
                    response.Content.ReadAsAsync<List<Status>>().Result;
                Assert.IsTrue(allStatusesOfUser.SequenceEqual(responseStatuses));
            }
            httpRequestMessage.Dispose();
        }
    }
}
